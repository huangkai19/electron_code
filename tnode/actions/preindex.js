// 在上下文隔离启用的情况下使用预加载
const { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('myAPI', {
  doAThing: () => {}
})

contextBridge.exposeInMainWorld('renderApi', {
    send: (channel, data) => {
        ipcRenderer.send(channel, data);
    },
    onEvent: (channel, callback, isOnce = false) => {
        if (isOnce) {
            ipcRenderer.once(channel, (event) => {
                console.log('ipc render once: ', event);
                callback && callback(event);
            })
        } else {
            ipcRenderer.on(channel, (event) => {
                console.log('ipc render on: ', event);
                callback && callback(event);
            })
        }
    }
})