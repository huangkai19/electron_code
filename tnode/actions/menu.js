const { Menu, shell } = require('electron');

const menu_templates = [
    {
        label: '帮助',
        submenu: [
            {
                label: '关于xxxx',
                click () {
                    shell.openExternal('https://blog.kenspace.xyz')
                }
            }
        ]
    },
    {
        label: '开发者工具',
        submenu: [
            {
                label: '打开',
                role: 'toggleDevTools'
            }
        ]
    },
]

const menu_build = Menu.buildFromTemplate(menu_templates);

module.exports = menu_build;