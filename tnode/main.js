const { app, BrowserWindow, Menu, ipcMain, globalShortcut, dialog } = require('electron');
const path = require('path');
const ACTION_PATH = path.join(__dirname, 'actions');
const PAGES_PATH = path.join(__dirname, 'pages');

const menuActions = require(ACTION_PATH + '/menu');

let win;

function createWindow() {
    win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            // nodeIntegration: true
            preload: ACTION_PATH + '/preindex.js'
        }
    });
    win.loadFile(PAGES_PATH + '/index.html');
    onMainEvent();
}

function onMainEvent() {
    ipcMain.on('hello', (data) => {
        console.log('main event', data);
    })
}

function registerGlobalShortcut() {
    const ret = globalShortcut.register('CommandOrControl+S', () => {
        console.log('main save');
        const focusWin = BrowserWindow.getFocusedWindow();
        // focusWin.webContents.send('save', '');
        const dialogOptions = {
            title: '保存文件'
        };

        dialog.showSaveDialog(focusWin, dialogOptions);
    })

    console.log('register', ret);

    console.log('Is register' + globalShortcut.isRegistered('CommandOrControl+S'))
}

// 初始化窗口
app.whenReady().then(() => {
    if (!win) {
        createWindow();
        registerGlobalShortcut();
    }
})

app.on('will-quit', () => {

    win = null;
    // 注销所有快捷键
    globalShortcut.unregisterAll()
})

// 设置菜单
Menu.setApplicationMenu(menuActions);