// 导入 electron 项目
const { app, BrowserWindow } = require('electron');
// 全局窗口对象
let win;

function _createWindow() {
    // 创建窗口
    win = new BrowserWindow({
        // 窗口大小
        width: 800,
        height: 600
    })

    win.loadFile('./pages/index.html');
}

app.whenReady().then(() => {
    
    _createWindow();

    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) {
            _createWindow()
        }
    })
})