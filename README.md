# electron_code

#### 介绍

Electron开发实践

#### 软件架构

- Node.js
- Typescript
- Vue
- React

#### 安装教程

1.  `npm install --save-dev electron`
2.  `yarn add --dev electron`

